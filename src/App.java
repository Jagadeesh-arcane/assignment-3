// Math Calculator Program

public class App {

    public static int add(int a, int b) {
        return a + b;
    }

    public static int subtract(int a, int b) {
        return a - b;
    }

    public static int multiply(int a, int b) {
        return a * b;
    }

    public static double divide(int a, int b) {
        if (b == 0) {
            throw new ArithmeticException("Cannot divide by zero");
        }
        return (double) a / b;
    }

    public static int cuberoot(int a) {
        return a * a * a;
    }

    public static int squreroot(int a) {
        return a * a;
    }

    public static void main(String[] args) {
        int num1 = 10;
        int num2 = 5;

        System.out.println("Addition: " + add(num1, num2));
        System.out.println("Subtraction: " + subtract(num1, num2));
        System.out.println("Multiplication: " + multiply(num1, num2));
        System.out.println("CubeRoot: " + cuberoot(num1));

        try {
            System.out.println("Division: " + divide(num1, num2));
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("SquareRoot: " + squreroot(num1));
    }

}

